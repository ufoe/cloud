# 图片验证码

##### 获取图片验证码

```js
const captcha = await module.captcha('uuid'); // uuid后台插件-验证码-图片验证码获取

const verify = captcha.text; // 验证码
ctx.log(verify);
ctx.body = captcha.data;
```