# 微信小程序支付

#### 获取微信支付参数

```js
const jsApiParameters = await module.wxapay({
  body: "吮指原味鸡 * 1", // 商品描述
  attach: "{'部位':'三角'}", // 附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
  out_trade_no: "kfc" + +new Date(), // 商户系统内部的订单号,32个字符内、可包含字母
  total_fee: parseFloat(0.01) * 100, // 订单总金额，单位为分
  spbill_create_ip: "127.0.0.1", // APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP
  openid: "oLh8Mw94s2dZeBeNZuAkGvkTczGM", // 用户标识
  trade_type: "JSAPI" // 交易类型，小程序取值如下：JSAPI
});
```

#### 微信小程序调用支付

```js
wx.requestPayment({
   'timeStamp': '',
   'nonceStr': '',
   'package': '',
   'signType': 'MD5',
   'paySign': '',
   'success':function(res){
   },
   'fail':function(res){
   }
})
```

#### 云函数支付成功回调

```js
const result = await module.wxapayNotifyValidate(); // 回调安全验证
ctx.log(result); // 获取微信支付返回的数据
ctx.log(result.out_trade_no); // 获取微信小程序支付订单号

if(result && result.return_code == "SUCCESS"){
  // 验证成功 && 支付成功
    // 业务逻辑 ToDo...
}
```