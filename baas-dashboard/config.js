const qiniu = require("qiniu");
const isDocker = require("is-docker");

module.exports = {
  port: 3000,

  version: "2.0",

  mysql: {
    host: isDocker() ? "mysql" : "127.0.0.1",
    user: "root",
    password: "xxx",
    database: "xxx",
    charset: "utf8"
  },

  socket: {
    port: 3001
  },

  redis: {
    host: isDocker() ? "redis" : "127.0.0.1",
    port: "6379"
  },

  init: [],

  middleware: [],

  // 分页数
  pageSize: 12,

  // jwt配置
  jwt: {
    secret: "xxx",
    expiresIn: "7 days"
  },

  // 验证码限制
  maxVerifyIp: 36,
  maxVerifyPhone: 6,

  // 短信配置
  appkey: "xxx",
  appsecret: "xxx",
  sms_free_sign_name: "xxx",
  sms_template_code: "xxx",
  sms_notice_template_code: "xxx",

  // 七牛配置
  qiniu: {
    accessKey: "xxx",
    secretKey: "xxx",
    bucket: "xxx",
    zone: qiniu.zone.Zone_z0,
    host: "xxx"
  },
  // 支付宝支付
  alipay: {
    account: "xxx",
    partner: "xxx",
    key: "xxx"
  },
  // baas地址
  baas: {
    url: "xxx"
  },
  // 验证码配置
  captcha: {
    noise: 4,
    color: false,
    ignoreChars: "0o1il",
    width: 100,
    height: 40
  },

  router: ["home/public.js", "home/base.js"],
  nginx: "/www/baas-vhosts",
  web: "/www/baas-web",
  maxFile: 5, // 最大文件限制为5M
  appidLength: 12, // appid长度
  noticeMoney: 12, // 提醒用户充值余额
  credits: -1 // 信用额度
};
