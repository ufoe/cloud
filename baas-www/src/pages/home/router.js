import util from "./../../util";
const Layout = resolve =>
    util.wrapComponent(require(["./layout.vue"], resolve));
const Common = resolve =>
    util.wrapComponent(require(["./app/layout.vue"], resolve));
const Empty = resolve => util.wrapComponent(require(["./empty.vue"], resolve));
//登录注册
const Login = resolve =>
    util.wrapComponent(require(["./public/login.vue"], resolve));
const Register = resolve =>
    util.wrapComponent(require(["./public/register.vue"], resolve));
const Password = resolve =>
    util.wrapComponent(require(["./public/password.vue"], resolve));

const App = resolve => util.wrapComponent(require(["./app/app.vue"], resolve));
const Money = resolve =>
    util.wrapComponent(require(["./money/money.vue"], resolve));
const Statistic = resolve =>
    util.wrapComponent(require(["./app/statistic/statistic.vue"], resolve));

const Info = resolve =>
    util.wrapComponent(require(["./account/info.vue"], resolve));
const Certify = resolve =>
    util.wrapComponent(require(["./account/certify.vue"], resolve));
const Coupon = resolve =>
    util.wrapComponent(require(["./account/coupon.vue"], resolve));

//数据
const DataIndex = resolve =>
    util.wrapComponent(require(["./app/data/data.vue"], resolve));
const TableIndex = resolve =>
    util.wrapComponent(require(["./app/data/table.vue"], resolve));
//关联
const Relation = resolve =>
    util.wrapComponent(require(["./app/schema/relation.vue"], resolve));
const Schema = resolve =>
    util.wrapComponent(require(["./app/schema/schema.vue"], resolve));
//权限
const AuthIndex = resolve =>
    util.wrapComponent(require(["./app/auth/auth.vue"], resolve));
const AuthKey = resolve =>
    util.wrapComponent(require(["./app/auth/key.vue"], resolve));
const AuthTable = resolve =>
    util.wrapComponent(require(["./app/auth/table.vue"], resolve));
const AuthClass = resolve =>
    util.wrapComponent(require(["./app/auth/class.vue"], resolve));
const AuthFields = resolve =>
    util.wrapComponent(require(["./app/auth/fields.vue"], resolve));
//文件
const FileIndex = resolve =>
    util.wrapComponent(require(["./app/file/file.vue"], resolve));
const FileFile = resolve =>
    util.wrapComponent(require(["./app/file/web.vue"], resolve));
//调试
const DebugIndex = resolve =>
    util.wrapComponent(require(["./app/engine/debug.vue"], resolve));
//引擎
const EngineFunc = resolve =>
    util.wrapComponent(require(["./app/engine/func.vue"], resolve));
const EngineTask = resolve =>
    util.wrapComponent(require(["./app/engine/task.vue"], resolve));
const EngineLog = resolve =>
    util.wrapComponent(require(["./app/engine/log.vue"], resolve));
//插件
const PluginIndex = resolve =>
    util.wrapComponent(require(["./app/plugin/plugin.vue"], resolve));
//验证码
const Doc = resolve =>
    util.wrapComponent(require(["./app/plugin/doc/doc.vue"], resolve));
const CaptchaSms = resolve =>
    util.wrapComponent(require(["./app/plugin/captcha/sms.vue"], resolve));
const CaptchaImage = resolve =>
    util.wrapComponent(require(["./app/plugin/captcha/image.vue"], resolve));
const Alipay = resolve =>
    util.wrapComponent(require(["./app/plugin/alipay/alipay.vue"], resolve));
const Wxpay = resolve =>
    util.wrapComponent(require(["./app/plugin/wxpay/wxpay.vue"], resolve));
const Wxa = resolve =>
    util.wrapComponent(require(["./app/plugin/wxa/wxa.vue"], resolve));
const Mail = resolve =>
    util.wrapComponent(require(["./app/plugin/mail/mail.vue"], resolve));
//设置
const ConfigIndex = resolve =>
    util.wrapComponent(require(["./app/config/config.vue"], resolve));
const Privilege = resolve =>
    util.wrapComponent(require(["./app/config/privilege.vue"], resolve));
const PrivilegeIndex = resolve =>
    util.wrapComponent(
        require(["./app/config/privilege/privilege.vue"], resolve)
    );

const Invite = resolve =>
    util.wrapComponent(require(["./app/invite.vue"], resolve));
const Notification = resolve =>
    util.wrapComponent(require(["./app/notification.vue"], resolve));

const routers = [
    {
        path: "/home",
        component: Empty,
        children: [
            {
                path: "public",
                component: Empty,
                children: [
                    {
                        path: "login",
                        component: Login
                    },
                    {
                        path: "register",
                        component: Register
                    },
                    {
                        path: "password",
                        component: Password
                    }
                ]
            }
        ]
    },
    {
        path: "/home",
        component: Layout,
        children: [
            {
                path: "dashboard",
                component: Empty,
                children: [
                    {
                        path: "app",
                        component: App
                    },
                    {
                        path: "money",
                        component: Money
                    },
                    {
                        path: "info",
                        component: Info
                    },
                    {
                        path: "certify",
                        component: Certify
                    },
                    {
                        path: "invite",
                        component: Invite
                    },
                    {
                        path: "notification",
                        component: Notification
                    },
                    {
                        path: "coupon",
                        component: Coupon
                    }
                ]
            }
        ]
    },

    {
        path: "/home",
        component: Common,
        children: [
            {
                path: "statistic",
                component: Empty,
                children: [
                    {
                        path: "statistic",
                        component: Statistic
                    }
                ]
            },
            {
                path: "data",
                component: Empty,
                children: [
                    {
                        path: "data",
                        component: DataIndex
                    },
                    {
                        path: "table",
                        component: TableIndex
                    }
                ]
            },
            {
                path: "schema",
                component: Empty,
                children: [
                    {
                        path: "schema",
                        component: Schema
                    },
                    {
                        path: "relation",
                        component: Relation
                    }
                ]
            },
            {
                path: "auth",
                component: Empty,
                children: [
                    {
                        path: "auth",
                        component: AuthIndex
                    },
                    {
                        path: "key",
                        component: AuthKey
                    },
                    {
                        path: "table",
                        component: AuthTable
                    },
                    {
                        path: "class",
                        component: AuthClass
                    },
                    {
                        path: "fields",
                        component: AuthFields
                    }
                ]
            },
            {
                path: "file",
                component: Empty,
                children: [
                    {
                        path: "file",
                        component: FileIndex
                    },
                    {
                        path: "web",
                        component: FileFile
                    }
                ]
            },
            {
                path: "engine",
                component: Empty,
                children: [
                    {
                        path: "func",
                        component: EngineFunc
                    },
                    {
                        path: "task",
                        component: EngineTask
                    },
                    {
                        path: "log",
                        component: EngineLog
                    },
                    {
                        path: "debug",
                        component: DebugIndex
                    }
                ]
            },
            {
                path: "plugin",
                component: Empty,
                children: [
                    {
                        path: "plugin",
                        component: PluginIndex
                    },
                    {
                        path: "doc",
                        component: Doc
                    },
                    {
                        path: "sms",
                        component: CaptchaSms
                    },
                    {
                        path: "image",
                        component: CaptchaImage
                    },
                    {
                        path: "alipay",
                        component: Alipay
                    },
                    {
                        path: "wxpay",
                        component: Wxpay
                    },
                    {
                        path: "wxa",
                        component: Wxa
                    },
                    {
                        path: "mail",
                        component: Mail
                    }
                ]
            },
            {
                path: "config",
                component: Empty,
                children: [
                    {
                        path: "config",
                        component: ConfigIndex
                    }
                ]
            },
            {
                path: "privilege",
                component: Empty,
                children: [
                    {
                        path: "privilege",
                        component: Privilege
                    },
                    {
                        path: ":id",
                        name: "privilege",
                        component: PrivilegeIndex
                    }
                ]
            }
        ]
    },
    {
        path: "*",
        redirect: "/home/dashboard/app"
    }
];
export default routers;
